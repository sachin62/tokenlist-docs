# Summary

-   [Introduction](./introduction.md)
-   [For Admin](./admin/home.md)
    -   [Login](./admin/queries/login.md)
    -   [Fetch All Creators](./admin/queries/list-all-creators.md)
    -   [Fetch All NFTs](./admin/queries/list-all-nfts.md)
    -   [Fetch All Wallets](./admin/queries/list-all-wallets.md)
    -   [Fetch Ledger Acc Details](./admin/queries/list-ledger-details.md)
    -   [Fetch All NFT Types](./admin/queries/list-all-nft-types.md)
    -   [Fetch NFT Type Details](./admin/queries/list-nft-type-details.md)
    -   [Add New Creator](./admin/mutations/add-new-creator.md)
    -   [Give Coins To Users](./admin/mutations/give-coins.md)
    -   [Update Creator Access](./admin/mutations/update-creator-access.md)
-   [For Dashboard](./dashboard/home.md)
    -   [Send OTP](./dashboard/mutations/send-otp.md)
    -   [Login With OTP](./dashboard/queries/login-with-otp.md)
    -   [Fetch All NFTs](./dashboard/queries/list-all-nfts.md)
    -   [Fetch All Own NFTs](./dashboard/queries/list-all-own-nfts.md)
    -   [Fetch NFT Details](./dashboard/queries/list-nft-details.md)
    -   [Fetch User Profile](./dashboard/queries/view-profile.md)
    -   [Fetch All Transactions](./dashboard/queries/list-all-transactions.md)
    -   [Fetch User Coins](./dashboard/queries/list-funq-credits.md)
    -   [Create New NFT](./dashboard/rest-apis/create-new-nft.md)
    -   [Send NFT To Email](./dashboard/mutations/send-nft-to-email.md)
    -   [Send NFT To Wallet](./dashboard/mutations/send-nft-to-wallet.md)
    -   [List NFTs In Marketplace](./dashboard/mutations/sell-nfts.md)
    -   [Redeem Wallet Amount](./dashboard/mutations/redeem-wallet-amount.md)
    -   [Delete NFT](./dashboard/mutations/delete-nft.md)
-   [For Marketplace](./marketplace/home.md)
    -   [Send OTP](./marketplace/send-otp.md)
    -   [Login With OTP](./marketplace/login-with-otp.md)
    -   [Fetch All Creators](./marketplace/list-all-creators.md)
    -   [Fetch All Creator NFTs](./marketplace/list-creator-nfts.md)
    -   [Fetch All User NFTs](./marketplace/list-all-user-nfts.md)
    -   [Fetch NFT Details](./marketplace/list-nft-details.md)
    -   [Fetch All Listed NFTs](./marketplace/list-all-selling-nfts.md)
