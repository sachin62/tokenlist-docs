# Add New Creator

> This API is for to add a new creator

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example              | Data Management |
| -------------- | ------ | --------------------------- | -------------------- | --------------- |
| URL            | string |                             | `{base_url}/admin`   |                 |
| GRAPHQL        | string |                             | **MUTATION**         |                 |
| content-type   | string | JSON                        | **application/json** |                 |
| x-access-token | string | session token with validity | **token**            | admin token     |

---

## Request

```json
mutation {
  add_new_creator(name: "sachin", wallet_id: "", email: "sachinsuresh704@gmail.com") {
    message
  }
}
```

---

## Request Parameters

| Fields    | Type   | Description                     | Required                    |
| --------- | ------ | ------------------------------- | --------------------------- |
| name      | string | name of the creator             | <div align="center">✘</div> |
| wallet_id | string | \_id of the non assigned wallet | <div align="center">✘</div> |
| email     | string | email of the creator            | <div align="center">✔</div> |

---

## Success

```json
{
    "data": {
        "add_new_creator": {
            "message": "creator added successfully"
        }
    }
}
```

---

## Error

```json
{
    "errors": [
        {
            "message": "Unauthorized",
            "locations": [
                {
                    "line": 2,
                    "column": 3
                }
            ],
            "path": ["add_new_creator"]
        }
    ],
    "data": {
        "add_new_creator": null
    }
}
```

---
