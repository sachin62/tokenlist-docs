# Give Coins To Users

> This API is for to give coins to users

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example              | Data Management |
| -------------- | ------ | --------------------------- | -------------------- | --------------- |
| URL            | string |                             | `{base_url}/admin`   |                 |
| GRAPHQL        | string |                             | **MUTATION**         |                 |
| content-type   | string | JSON                        | **application/json** |                 |
| x-access-token | string | session token with validity | **token**            | admin token     |

---

## Request

```json
mutation {
  add_funq_credits(credits: 10, user_id: "62edda58bdeee16f97d14b18") {
    message
  }
}
```

---

## Request Parameters

| Fields  | Type    | Description                           | Required                    |
| ------- | ------- | ------------------------------------- | --------------------------- |
| credits | integer | The number of coins the user receives | <div align="center">✔</div> |
| user_id | string  | \_id of the user                      | <div align="center">✔</div> |

---

## Success

```json
{
    "data": {
        "add_funq_credits": {
            "message": "credits added successfully"
        }
    }
}
```

---

## Error

```json
{
    "errors": [
        {
            "message": "Unauthorized",
            "locations": [
                {
                    "line": 2,
                    "column": 3
                }
            ],
            "path": ["add_funq_credits"]
        }
    ],
    "data": {
        "add_funq_credits": null
    }
}
```

---
