# Update Creator Access Of Users

> This API is for to update creator access of users

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example              | Data Management |
| -------------- | ------ | --------------------------- | -------------------- | --------------- |
| URL            | string |                             | `{base_url}/admin`   |                 |
| GRAPHQL        | string |                             | **MUTATION**         |                 |
| content-type   | string | JSON                        | **application/json** |                 |
| x-access-token | string | session token with validity | **token**            | admin token     |

---

## Request

```json
mutation {
  update_creator_access(user_id: "62edda58bdeee16f97d14b18") {
    message
  }
}
```

---

## Request Parameters

| Fields  | Type   | Description      | Required                    |
| ------- | ------ | ---------------- | --------------------------- |
| user_id | string | \_id of the user | <div align="center">✔</div> |

---

## Success

```json
{
    "data": {
        "update_creator_access": {
            "message": "updated successfully"
        }
    }
}
```

---

## Error

```json
{
    "errors": [
        {
            "message": "Unauthorized",
            "locations": [
                {
                    "line": 2,
                    "column": 3
                }
            ],
            "path": ["update_creator_access"]
        }
    ],
    "data": {
        "update_creator_access": null
    }
}
```

---
