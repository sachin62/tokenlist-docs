# Fetch All Creators

> This API is for to get list of all creators

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example              | Data Management |
| -------------- | ------ | --------------------------- | -------------------- | --------------- |
| URL            | string |                             | `{base_url}/admin`   |                 |
| GRAPHQL        | string |                             | **QUERY**            |                 |
| content-type   | string | JSON                        | **application/json** |                 |
| x-access-token | string | session token with validity | **token**            | admin token     |

---

## Request

```json
{
  list_all_creators(page_size: 10, page_number: 1) {
    message
    data {
      _id
      name
      email
      img
      wallet_id {
        _id
        wallet_id
      }
    }
    hasMore
  }
}

```

---

## Request Parameters

| Fields      | Type    | Description                          | Required                    |
| ----------- | ------- | ------------------------------------ | --------------------------- |
| page_size   | integer | total number of data to be displayed | <div align="center">✘</div> |
| page_number | integer | page number to filter data           | <div align="center">✘</div> |

---

## Success

```json
{
    "data": {
        "list_all_creators": {
            "message": "data listed",
            "data": [
                {
                    "_id": "62299c9254f6c2bcd4b2f09a",
                    "name": "Sachin T S",
                    "email": "sachinsuresh704@gmail.com",
                    "img": null,
                    "wallet_id": {
                        "_id": "6229df0fcb68a946e41b8421",
                        "wallet_id": "EnbSFnpMCQz7NzmfuKpPF1u4B8htaA19iYVkkrJi1aPy"
                    }
                },
                {
                    "_id": "62469e32f63e08df6e01159f",
                    "name": "jimshad",
                    "email": "jimshaddb@gmail.com",
                    "img": null,
                    "wallet_id": {
                        "_id": "6246b6eef63e08baf90115b8",
                        "wallet_id": "HrVUNq8PyKwym8dTwdBe7gHmaV3UWBkxztmBttHKPQX3"
                    }
                }
            ],
            "hasMore": false
        }
    }
}
```

---

## Error

```json
{
    "errors": [
        {
            "message": "Unauthorized",
            "locations": [
                {
                    "line": 2,
                    "column": 3
                }
            ],
            "path": ["list_all_creators"]
        }
    ],
    "data": {
        "list_all_creators": null
    }
}
```

---
