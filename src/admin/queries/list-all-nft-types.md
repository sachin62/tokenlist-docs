# Fetch All NFT Types

> This API is for to get list of all NFT types

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example              | Data Management |
| -------------- | ------ | --------------------------- | -------------------- | --------------- |
| URL            | string |                             | `{base_url}/admin`   |                 |
| GRAPHQL        | string |                             | **QUERY**            |                 |
| content-type   | string | JSON                        | **application/json** |                 |
| x-access-token | string | session token with validity | **token**            | admin token     |

---

## Request

```json
{
  list_all_nft_types(page_size: 10, page_number: 1) {
    message
    data {
      _id
      name
      description
      qr_based
      is_visible
    }
    hasMore
  }
}
```

---

## Request Parameters

| Fields      | Type    | Description                          | Required                    |
| ----------- | ------- | ------------------------------------ | --------------------------- |
| page_size   | integer | total number of data to be displayed | <div align="center">✘</div> |
| page_number | integer | page number to filter data           | <div align="center">✘</div> |

---

## Success

```json
{
    "data": {
        "list_all_nft_types": {
            "message": "data listed",
            "data": [
                {
                    "_id": "62edda58bdeee16f97d14b18",
                    "name": "equity shares",
                    "description": "secondary marketplace for buyers and sellers of India’s hottest startups and privately held companies",
                    "qr_based": true,
                    "is_visible": true
                },
                {
                    "_id": "62edd8f7bdeee1d2bbd14b0a",
                    "name": "agriculture land",
                    "description": "Marketplace to make food trade easy, fast and transparent",
                    "qr_based": true,
                    "is_visible": true
                },
                {
                    "_id": "62edd7eebdeee110dcd14b00",
                    "name": "event tickets",
                    "description": "Buy and sell tickets online for concerts, sports, theater, family and other events near you.",
                    "qr_based": true,
                    "is_visible": true
                }
            ],
            "hasMore": false
        }
    }
}
```

---

## Error

```json
{
    "errors": [
        {
            "message": "Unauthorized",
            "locations": [
                {
                    "line": 2,
                    "column": 3
                }
            ],
            "path": ["list_all_nft_types"]
        }
    ],
    "data": {
        "list_all_nft_types": null
    }
}
```

---
