# Fetch All NFTs

> This API is for to get list of all NFts

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example              | Data Management |
| -------------- | ------ | --------------------------- | -------------------- | --------------- |
| URL            | string |                             | `{base_url}/admin`   |                 |
| GRAPHQL        | string |                             | **QUERY**            |                 |
| content-type   | string | JSON                        | **application/json** |                 |
| x-access-token | string | session token with validity | **token**            | admin token     |

---

## Request

```json
{
  list_all_nfts(page_size: 10, page_number: 1, status: "all", creator: "") {
    message
    data {
      _id
      name
      description
      art_url
      royalty_percentage
      nft_id
      status
      is_sold
      creator_id {
        _id
        name
        email
      }
    }
    hasMore
  }
}


```

---

## Request Parameters

| Fields      | Type    | Description                                                  | Required                    |
| ----------- | ------- | ------------------------------------------------------------ | --------------------------- |
| page_size   | integer | total number of data to be displayed                         | <div align="center">✘</div> |
| page_number | integer | page number to filter data                                   | <div align="center">✘</div> |
| status      | string  | to filter data based on NFT status                           | <div align="center">✘</div> |
| creator     | string  | \_id of the creator to get NFTs created by the given creator | <div align="center">✘</div> |

---

## Success

```json
{
    "data": {
        "list_all_nfts": {
            "message": "data listed",
            "data": [
                {
                    "_id": "624fbdc6521415d06a5087cc",
                    "name": "Boy",
                    "description": "Cartoon character",
                    "art_url": "https://nft-store-assets.s3.amazonaws.com/4eb0dd60-597c-4291-b19e-75e6e378289d.png",
                    "royalty_percentage": 0,
                    "nft_id": "FWz7BCZvkJiib9K7NuNGaH12xa51ggZesoqENaM7We9L",
                    "status": "sold",
                    "is_sold": true,
                    "creator_id": {
                        "_id": "62299c9254f6c2bcd4b2f09a",
                        "name": "Sachin T S",
                        "email": "sachinsuresh704@gmail.com"
                    }
                },
                {
                    "_id": "624fbf7a52141537ef508815",
                    "name": "Man with Glasses",
                    "description": "Man cartoon",
                    "art_url": "https://nft-store-assets.s3.amazonaws.com/2c408c83-0000-48fe-9ea5-d60c4251e360.png",
                    "royalty_percentage": 1,
                    "nft_id": "56QzcVEztAGFXr17ow3SBvmPvt7rKaQzMN8d4Ny3zzSo",
                    "status": "sold",
                    "is_sold": true,
                    "creator_id": {
                        "_id": "62299c9254f6c2bcd4b2f09a",
                        "name": "Sachin T S",
                        "email": "sachinsuresh704@gmail.com"
                    }
                }
            ],
            "hasMore": true
        }
    }
}
```

---

## Error

```json
{
    "errors": [
        {
            "message": "Unauthorized",
            "locations": [
                {
                    "line": 2,
                    "column": 3
                }
            ],
            "path": ["list_all_nfts"]
        }
    ],
    "data": {
        "list_all_nfts": null
    }
}
```

---
