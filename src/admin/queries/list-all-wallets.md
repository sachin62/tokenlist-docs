# Fetch All Wallets

> This API is for to get list of all wallets

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example              | Data Management |
| -------------- | ------ | --------------------------- | -------------------- | --------------- |
| URL            | string |                             | `{base_url}/admin`   |                 |
| GRAPHQL        | string |                             | **QUERY**            |                 |
| content-type   | string | JSON                        | **application/json** |                 |
| x-access-token | string | session token with validity | **token**            | admin token     |

---

## Request

```json
{
  list_all_wallets(page_size: 2, page_number: 1) {
    message
    data {
      _id
      wallet_id
      is_linked
      user_id {
        _id
        name
        email
      }
    }
    hasMore
  }
}

```

---

## Request Parameters

| Fields      | Type    | Description                          | Required                    |
| ----------- | ------- | ------------------------------------ | --------------------------- |
| page_size   | integer | total number of data to be displayed | <div align="center">✘</div> |
| page_number | integer | page number to filter data           | <div align="center">✘</div> |

---

## Success

```json
{
    "data": {
        "list_all_wallets": {
            "message": "data listed",
            "data": [
                {
                    "_id": "62579d28ba8a2b819c325d94",
                    "wallet_id": "4nWNM7zdy9gtgpiURL1e9e8SmEneUHGx3qnEnqWBF5Qr",
                    "is_linked": true,
                    "user_id": {
                        "_id": "624fcb2b521415c21a508ac5",
                        "name": "Nicky Jacob",
                        "email": "nickyjacob@gmail.com"
                    }
                },
                {
                    "_id": "6246b6eef63e08baf90115b8",
                    "wallet_id": "HrVUNq8PyKwym8dTwdBe7gHmaV3UWBkxztmBttHKPQX3",
                    "is_linked": true,
                    "user_id": {
                        "_id": "62469e32f63e08df6e01159f",
                        "name": "jimshad",
                        "email": "jimshaddb@gmail.com"
                    }
                }
            ],
            "hasMore": true
        }
    }
}
```

---

## Error

```json
{
    "errors": [
        {
            "message": "Unauthorized",
            "locations": [
                {
                    "line": 2,
                    "column": 3
                }
            ],
            "path": ["list_all_wallets"]
        }
    ],
    "data": {
        "list_all_wallets": null
    }
}
```

---
