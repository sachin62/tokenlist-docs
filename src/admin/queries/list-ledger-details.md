# Fetch Ledger Wallet Details

> This API is for to get details of the ledger wallet

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example              | Data Management |
| -------------- | ------ | --------------------------- | -------------------- | --------------- |
| URL            | string |                             | `{base_url}/admin`   |                 |
| GRAPHQL        | string |                             | **QUERY**            |                 |
| content-type   | string | JSON                        | **application/json** |                 |
| x-access-token | string | session token with validity | **token**            | admin token     |

---

## Request

```json
{
  list_ledger_details {
    wallet_address
    wallet_balance
  }
}

```

---

## Success

```json
{
    "data": {
        "list_ledger_details": {
            "wallet_address": "HMo5HENvpN42pBoeXLG5kwHa4XTBdjxUmkPYTyQjNkft",
            "wallet_balance": 23.3
        }
    }
}
```

---

## Error

```json
{
    "errors": [
        {
            "message": "Unauthorized",
            "locations": [
                {
                    "line": 2,
                    "column": 2
                }
            ],
            "path": ["list_ledger_details"]
        }
    ],
    "data": {
        "list_ledger_details": null
    }
}
```

---
