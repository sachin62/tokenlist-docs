# Fetch NFT Type Details

> This API provides information about NFT type

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example              | Data Management |
| -------------- | ------ | --------------------------- | -------------------- | --------------- |
| URL            | string |                             | `{base_url}/admin`   |                 |
| GRAPHQL        | string |                             | **QUERY**            |                 |
| content-type   | string | JSON                        | **application/json** |                 |
| x-access-token | string | session token with validity | **token**            | admin token     |

---

## Request

```json
{
  list_nft_type_details(_id: "62edda58bdeee16f97d14b18") {
    message
    data {
      _id
      name
      description
      qr_based
      is_visible
      fields {
        _id
        min
        max
        is_visible
        is_nft_name
      }
    }
  }
}
```

---

## Request Parameters

| Fields | Type   | Description          | Required                    |
| ------ | ------ | -------------------- | --------------------------- |
| \_id   | string | \_id of the NFT type | <div align="center">✔</div> |

---

## Success

```json
{
    "data": {
        "list_nft_type_details": {
            "message": "data listed",
            "data": {
                "_id": "62edda58bdeee16f97d14b18",
                "name": "equity shares",
                "description": "secondary marketplace for buyers and sellers of India’s hottest startups and privately held companies",
                "qr_based": true,
                "is_visible": true,
                "fields": [
                    {
                        "_id": "62edda58bdeee1af4ad14b19",
                        "name": "company_name",
                        "type": "text",
                        "min": null,
                        "max": null,
                        "is_visible": true,
                        "is_nft_name": true
                    },
                    {
                        "_id": "62edda58bdeee10b38d14b1a",
                        "name": "company_type",
                        "type": "text",
                        "min": null,
                        "max": null,
                        "is_visible": true,
                        "is_nft_name": false
                    },
                    {
                        "_id": "62edda58bdeee1e4d9d14b1b",
                        "name": "company_id",
                        "type": "text",
                        "min": null,
                        "max": null,
                        "is_visible": true,
                        "is_nft_name": false
                    }
                ]
            }
        }
    }
}
```

---

## Error

```json
{
    "errors": [
        {
            "message": "Unauthorized",
            "locations": [
                {
                    "line": 2,
                    "column": 3
                }
            ],
            "path": ["list_nft_type_details"]
        }
    ],
    "data": {
        "list_nft_type_details": null
    }
}
```

---
