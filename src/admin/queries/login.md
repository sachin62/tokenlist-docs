# Login

> This API is for login to admin account

---

### GRAPHQL

---

## Headers

| Fields       | Type   | Description | Example              | Data Management |
| ------------ | ------ | ----------- | -------------------- | --------------- |
| URL          | string |             | `{base_url}/admin`   |                 |
| GRAPHQL      | string |             | **QUERY**            |                 |
| content-type | string | JSON        | **application/json** |                 |

---

## Request

```json
{
  admin_login(email: "nftadmin@gmail.com", password: "nftadmin@123") {
    message
    token
  }
}


```

---

## Request Parameters

| Fields   | Type   | Description           | Required                    |
| -------- | ------ | --------------------- | --------------------------- |
| email    | string | email of the admin    | <div align="center">✔</div> |
| password | string | password of the admin | <div align="center">✔</div> |

---

## Success

```json
{
    "data": {
        "admin_login": {
            "message": "Login success",
            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiYWRtaW4iLCJhY2NvdW50VHlwZSI6ImFkbWluIiwiaWF0IjoxNjUyMzI4MjAyLCJleHAiOjE2NTIzNDk4MDJ9.qPBFexWB-bYK3P5o9SvoJIiUABnJJ80baKqfw8Uj6Hw"
        }
    }
}
```

---

## Error

```json
{
    "errors": [
        {
            "message": "Incorrect email address",
            "locations": [
                {
                    "line": 2,
                    "column": 3
                }
            ],
            "path": ["admin_login"]
        }
    ],
    "data": {
        "admin_login": null
    }
}
```

\
\

```json
{
    "errors": [
        {
            "message": "Incorrect password",
            "locations": [
                {
                    "line": 2,
                    "column": 3
                }
            ],
            "path": ["admin_login"]
        }
    ],
    "data": {
        "admin_login": null
    }
}
```

---
