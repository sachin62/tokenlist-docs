# Delete a NFT

> The API allows you to delete a NFT

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example               | Data Management |
| -------------- | ------ | --------------------------- | --------------------- | --------------- |
| URL            | string |                             | `{base_url}/creators` |                 |
| GRAPHQL        | string |                             | **MUTATION**          |                 |
| content-type   | string | JSON                        | **application/json**  |                 |
| x-access-token | string | session token with validity | **token**             | creator token   |

---

## Request

```json
mutation {
  delete_nft(nft_id: "624fc253521415614f508933") {
    message
  }
}
```

---

## Request Parameters

| Fields    | Type   | Description     | Required                    |
| --------- | ------ | --------------- | --------------------------- |
| nft\__id_ | string | \_id of the NFT | <div align="center">✔</div> |

---

## Success

```json
{
    "data": {
        "delete_nft": {
            "message": "deleted successfully"
        }
    }
}
```

---

## Error

```json
{
    "errors": [
        {
            "message": "Unauthorized",
            "locations": [
                {
                    "line": 2,
                    "column": 3
                }
            ],
            "path": ["delete_nft"]
        }
    ],
    "data": {
        "delete_nft": null
    }
}
```

---
