# Redeem Wallet Amount

> This API allows you to redeem wallet balances into another wallet

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example               | Data Management |
| -------------- | ------ | --------------------------- | --------------------- | --------------- |
| URL            | string |                             | `{base_url}/creators` |                 |
| GRAPHQL        | string |                             | **MUTATION**          |                 |
| content-type   | string | JSON                        | **application/json**  |                 |
| x-access-token | string | session token with validity | **token**             | creator token   |

---

## Request

```json
mutation {
  redeem_wallet_amount(dest_wallet_id: "6Qq7ERSHTZGnPpWarzbf1ZU9AMTqJgdj7WA16eSRiyZ4", amount: 0.5) {
    message
  }
}
```

---

## Request Parameters

| Fields         | Type   | Description                  | Required                    |
| -------------- | ------ | ---------------------------- | --------------------------- |
| dest_wallet_id | string | public address of the wallet | <div align="center">✔</div> |
| amount         | float  | amount to redeem             | <div align="center">✔</div> |

---

## Success

```json
{
    "data": {
        "redeem_wallet_amount": {
            "message": "redeemed successfully"
        }
    }
}
```

---

## Error

```json
{
    "errors": [
        {
            "message": "Unauthorized",
            "locations": [
                {
                    "line": 2,
                    "column": 3
                }
            ],
            "path": ["redeem_wallet_amount"]
        }
    ],
    "data": {
        "redeem_wallet_amount": null
    }
}
```

---
