# List NFTs in Marketplace

> Using the API, you can list NFTs on the marketplace

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example               | Data Management |
| -------------- | ------ | --------------------------- | --------------------- | --------------- |
| URL            | string |                             | `{base_url}/creators` |                 |
| GRAPHQL        | string |                             | **MUTATION**          |                 |
| content-type   | string | JSON                        | **application/json**  |                 |
| x-access-token | string | session token with validity | **token**             | creator token   |

---

## Request

```json
mutation {
  sell_nfts(nft_data: ["2iRD8A88h1SRkK1Hhra53jqnfEkhXx5mK58PvLb5YTRW", "J7jzYCEi3sFdiH8vs14Saabw3joB8rFzTuraMQecxNpg"], amount: 1000) {
    message
  }
}

```

---

## Request Parameters

| Fields   | Type  | Description                         | Required                    |
| -------- | ----- | ----------------------------------- | --------------------------- |
| nft_data | array | the NFT's public address            | <div align="center">✔</div> |
| amount   | float | Listing the amount in Indian rupees | <div align="center">✔</div> |

---

## Success

```json
{
    "data": {
        "sell_nfts": {
            "message": "Artworks added for sales"
        }
    }
}
```

---

## Error

```json
{
    "errors": [
        {
            "message": "Unauthorized",
            "locations": [
                {
                    "line": 2,
                    "column": 2
                }
            ],
            "path": ["sell_nfts"]
        }
    ],
    "data": {
        "sell_nfts": null
    }
}
```

---
