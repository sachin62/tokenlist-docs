# Send NFT To Email

> The API allows you to send a NFT via email to another user

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example               | Data Management |
| -------------- | ------ | --------------------------- | --------------------- | --------------- |
| URL            | string |                             | `{base_url}/creators` |                 |
| GRAPHQL        | string |                             | **MUTATION**          |                 |
| content-type   | string | JSON                        | **application/json**  |                 |
| x-access-token | string | session token with validity | **token**             | creator token   |

---

## Request

```json
mutation {
  send_nft_to_email(email: "sachinsuresh704@gmail.com", name: "sachin", nft_id: "624fc253521415614f508933") {
    message
  }
}

```

---

## Request Parameters

| Fields | Type   | Description       | Required                    |
| ------ | ------ | ----------------- | --------------------------- |
| email  | string | email of the user | <div align="center">✔</div> |
| name   | string | name of the user  | <div align="center">✘</div> |
| nft_id | string | \_id of the nft   | <div align="center">✔</div> |

---

## Success

```json
{
    "data": {
        "send_nft_to_email": {
            "message": "NFT sent successfully"
        }
    }
}
```

---

## Error

```json
{
    "errors": [
        {
            "message": "Unauthorized",
            "locations": [
                {
                    "line": 2,
                    "column": 3
                }
            ],
            "path": ["send_nft_to_email"]
        }
    ],
    "data": {
        "send_nft_to_email": null
    }
}
```

---
