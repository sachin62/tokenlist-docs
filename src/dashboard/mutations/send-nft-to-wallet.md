# Send NFT To Wallet

> The API allows you to send a NFT via solana wallet address to another user

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example               | Data Management |
| -------------- | ------ | --------------------------- | --------------------- | --------------- |
| URL            | string |                             | `{base_url}/creators` |                 |
| GRAPHQL        | string |                             | **MUTATION**          |                 |
| content-type   | string | JSON                        | **application/json**  |                 |
| x-access-token | string | session token with validity | **token**             | creator token   |

---

## Request

```json
mutation {
  send_nft_to_wallet(nft_id: "2iRD8A88h1SRkK1Hhra53jqnfEkhXx5mK58PvLb5YTRW", dest_wallet_id: "6Qq7ERSHTZGnPpWarzbf1ZU9AMTqJgdj7WA16eSRiyZ4") {
    message
  }
}


```

---

## Request Parameters

| Fields         | Type   | Description                  | Required                    |
| -------------- | ------ | ---------------------------- | --------------------------- |
| dest_wallet_id | string | public address of the wallet | <div align="center">✔</div> |
| nft_id         | string | public address of the nft    | <div align="center">✔</div> |

---

## Success

```json
{
    "data": {
        "send_nft_to_wallet": {
            "message": "NFT sent successfully"
        }
    }
}
```

---

## Error

```json
{
    "errors": [
        {
            "message": "Unauthorized",
            "locations": [
                {
                    "line": 2,
                    "column": 3
                }
            ],
            "path": ["send_nft_to_wallet"]
        }
    ],
    "data": {
        "send_nft_to_wallet": null
    }
}
```

---
