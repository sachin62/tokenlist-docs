# Send OTP

> This API is for to send an OTP to the email address for login

---

### GRAPHQL

---

## Headers

| Fields       | Type   | Description | Example               | Data Management |
| ------------ | ------ | ----------- | --------------------- | --------------- |
| URL          | string |             | `{base_url}/creators` |                 |
| GRAPHQL      | string |             | **MUTATION**          |                 |
| content-type | string | JSON        | **application/json**  |                 |

---

## Request

```json
mutation {
  send_otp(email: "sachinsuresh704@gmail.com") {
    message
  }
}

```

---

## Request Parameters

| Fields | Type   | Description       | Required                    |
| ------ | ------ | ----------------- | --------------------------- |
| email  | string | email of the user | <div align="center">✔</div> |

---

## Success

```json
{
    "data": {
        "send_otp": {
            "message": "OTP sent successfully"
        }
    }
}
```

---

## Error

```json

```

---
