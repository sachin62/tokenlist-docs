# Fetch All NFTs

> This API is for to get list of all NFTs

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example               | Data Management |
| -------------- | ------ | --------------------------- | --------------------- | --------------- |
| URL            | string |                             | `{base_url}/creators` |                 |
| GRAPHQL        | string |                             | **QUERY**             |                 |
| content-type   | string | JSON                        | **application/json**  |                 |
| x-access-token | string | session token with validity | **token**             | creator token   |

---

## Request

```json
{
  list_all_nfts(page_size: 2, page_number: 1, status: "all") {
    message
    data {
      _id
      name
      description
      art_url
      nft_id
      status
    }
    hasMore
  }
}

```

---

## Request Parameters

| Fields      | Type    | Description                          | Required                    |
| ----------- | ------- | ------------------------------------ | --------------------------- |
| page_size   | integer | total number of data to be displayed | <div align="center">✘</div> |
| page_number | integer | page number to filter data           | <div align="center">✘</div> |
| status      | string  | to filter data based on NFT status   | <div align="center">✘</div> |

---

## Success

```json
{
    "data": {
        "list_all_nfts": {
            "message": "data listed",
            "data": [
                {
                    "_id": "624fc253521415614f508933",
                    "name": "Old man",
                    "description": "Old man cartoon character",
                    "art_url": "https://nft-store-assets.s3.amazonaws.com/033d711b-5554-47c2-8c7f-41e935cab405.png",
                    "nft_id": "2iRD8A88h1SRkK1Hhra53jqnfEkhXx5mK58PvLb5YTRW",
                    "status": "sold"
                },
                {
                    "_id": "6253ec74fb9f6b511cf05fbf",
                    "name": "Boy",
                    "description": "cartoon boy",
                    "art_url": "https://nft-store-assets.s3.amazonaws.com/0c3a1492-d05a-4299-ac87-44a9a6c72950.png",
                    "nft_id": "J7jzYCEi3sFdiH8vs14Saabw3joB8rFzTuraMQecxNpg",
                    "status": "sold"
                }
            ],
            "hasMore": true
        }
    }
}
```

---

## Error

```json
{
    "errors": [
        {
            "message": "Unauthorized",
            "locations": [
                {
                    "line": 2,
                    "column": 3
                }
            ],
            "path": ["list_all_nfts"]
        }
    ],
    "data": {
        "list_all_nfts": null
    }
}
```

---
