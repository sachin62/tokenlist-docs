# Fetch All Transactions

> This API is for to get list of all Transactions

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example               | Data Management |
| -------------- | ------ | --------------------------- | --------------------- | --------------- |
| URL            | string |                             | `{base_url}/creators` |                 |
| GRAPHQL        | string |                             | **QUERY**             |                 |
| content-type   | string | JSON                        | **application/json**  |                 |
| x-access-token | string | session token with validity | **token**             | creator token   |

---

## Request

```json
{
  list_all_transactions(page_size: 2, page_number: 1, tag: "nft") {
    message
    data {
      _id
      txn_id
      type
      tag
      nft_id
      status
      amount
      to
      from
    }
    hasMore
  }
}


```

---

## Request Parameters

| Fields      | Type    | Description                          | Required                    |
| ----------- | ------- | ------------------------------------ | --------------------------- |
| page_size   | integer | total number of data to be displayed | <div align="center">✘</div> |
| page_number | integer | page number to filter data           | <div align="center">✘</div> |
| tag         | string  | to filter data based on txn type     | <div align="center">✘</div> |

---

## Success

```json
{
    "data": {
        "list_all_transactions": {
            "message": "data listed",
            "data": [
                {
                    "_id": "62789ef3c048d385d34dc603",
                    "txn_id": "7305e946-cca5-4904-9694-10474e66a364",
                    "type": "sold",
                    "tag": "nft",
                    "nft_id": "2iRD8A88h1SRkK1Hhra53jqnfEkhXx5mK58PvLb5YTRW",
                    "status": "success",
                    "amount": 0.04,
                    "to": "sachinsuresh704@gmail.com",
                    "from": "9b3RBztwiSM4DFvoHtr6nENRZxfQkmJcfxY4VSWVSBrH"
                },
                {
                    "_id": "627899c6c048d306984dc5ce",
                    "txn_id": "eb8c0491-8cea-4dce-8e7b-161c4682d641",
                    "type": "sold",
                    "tag": "nft",
                    "nft_id": "J7jzYCEi3sFdiH8vs14Saabw3joB8rFzTuraMQecxNpg",
                    "status": "success",
                    "amount": 0.03,
                    "to": "sachinsuresh704@gmail.com",
                    "from": "9b3RBztwiSM4DFvoHtr6nENRZxfQkmJcfxY4VSWVSBrH"
                }
            ],
            "hasMore": true
        }
    }
}
```

---

## Error

```json
{
    "errors": [
        {
            "message": "Unauthorized",
            "locations": [
                {
                    "line": 2,
                    "column": 2
                }
            ],
            "path": ["list_all_transactions"]
        }
    ],
    "data": {
        "list_all_transactions": null
    }
}
```

---
