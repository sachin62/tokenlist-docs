# Fetch Funq Credits

> This API is for to get total funq credits available for the user

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example               | Data Management |
| -------------- | ------ | --------------------------- | --------------------- | --------------- |
| URL            | string |                             | `{base_url}/creators` |                 |
| GRAPHQL        | string |                             | **QUERY**             |                 |
| content-type   | string | JSON                        | **application/json**  |                 |
| x-access-token | string | session token with validity | **token**             | creator token   |

---

## Request

```json
{
  show_funq_credits {
    message
    data {
      credits
    }
  }
}

```

---

## Success

```json
{
    "data": {
        "show_funq_credits": {
            "message": "data listed",
            "data": {
                "credits": 35
            }
        }
    }
}
```

---

## Error

```json
{
    "errors": [
        {
            "message": "Unauthorized",
            "locations": [
                {
                    "line": 2,
                    "column": 3
                }
            ],
            "path": ["show_funq_credits"]
        }
    ],
    "data": {
        "show_funq_credits": null
    }
}
```

---
