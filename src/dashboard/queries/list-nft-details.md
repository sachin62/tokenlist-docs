# Fetch NFT Details

> This API is for to get details of an NFT

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example               | Data Management |
| -------------- | ------ | --------------------------- | --------------------- | --------------- |
| URL            | string |                             | `{base_url}/creators` |                 |
| GRAPHQL        | string |                             | **QUERY**             |                 |
| content-type   | string | JSON                        | **application/json**  |                 |
| x-access-token | string | session token with validity | **token**             | creator token   |

---

## Request

```json
{
  list_nft_details(_id: "624fc253521415614f508933") {
    message
    data {
      _id
      name
      description
      update_authority
      art_url
      royalty_percentage
      nft_id
      is_for_sale
      selling_price
      status
      creator_id {
        _id
        name
        email
      }
      owner_id {
        _id
        name
        email
      }
    }
  }
}

```

---

## Request Parameters

| Fields | Type   | Description     | Required                    |
| ------ | ------ | --------------- | --------------------------- |
| \_id   | string | \_id of the NFT | <div align="center">✔</div> |

---

## Success

```json
{
    "data": {
        "list_nft_details": {
            "message": "data listed",
            "data": {
                "_id": "624fc253521415614f508933",
                "name": "Old man",
                "description": "Old man cartoon character",
                "update_authority": "62299c9254f6c2bcd4b2f09a",
                "art_url": "https://nft-store-assets.s3.amazonaws.com/033d711b-5554-47c2-8c7f-41e935cab405.png",
                "royalty_percentage": 0,
                "nft_id": "2iRD8A88h1SRkK1Hhra53jqnfEkhXx5mK58PvLb5YTRW",
                "is_for_sale": false,
                "selling_price": 0,
                "status": "sold",
                "creator_id": {
                    "_id": "62299c9254f6c2bcd4b2f09a",
                    "name": "Sachin T S",
                    "email": "sachinsuresh704@gmail.com"
                },
                "owner_id": {
                    "_id": "62299c9254f6c2bcd4b2f09a",
                    "name": "Sachin T S",
                    "email": "sachinsuresh704@gmail.com"
                }
            }
        }
    }
}
```

---

## Error

```json
{
    "errors": [
        {
            "message": "art not found",
            "locations": [
                {
                    "line": 2,
                    "column": 3
                }
            ],
            "path": ["list_nft_details"]
        }
    ],
    "data": {
        "list_nft_details": null
    }
}
```

```json
{
    "errors": [
        {
            "message": "Unauthorized",
            "locations": [
                {
                    "line": 2,
                    "column": 3
                }
            ],
            "path": ["list_nft_details"]
        }
    ],
    "data": {
        "list_nft_details": null
    }
}
```

---
