# Fetch User Profile

> This API is for to get profile details of a users

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example               | Data Management |
| -------------- | ------ | --------------------------- | --------------------- | --------------- |
| URL            | string |                             | `{base_url}/creators` |                 |
| GRAPHQL        | string |                             | **QUERY**             |                 |
| content-type   | string | JSON                        | **application/json**  |                 |
| x-access-token | string | session token with validity | **token**             | creator token   |

---

## Request

```json
{
  view_profile {
    message
    data {
      _id
      name
      email
      img
      is_creator
      acc_balance
      funq_credits
      wallet_address
    }
  }
}


```

---

## Success

```json
{
    "data": {
        "view_profile": {
            "message": "data listed",
            "data": {
                "_id": "62299c9254f6c2bcd4b2f09a",
                "name": "Sachin T S",
                "email": "sachinsuresh704@gmail.com",
                "img": null,
                "is_creator": true,
                "acc_balance": 2125523918.95,
                "funq_credits": 35,
                "wallet_address": "EnbSFnpMCQz7NzmfuKpPF1u4B8htaA19iYVkkrJi1aPy"
            }
        }
    }
}
```

---

## Error

```json
{
    "errors": [
        {
            "message": "Unauthorized",
            "locations": [
                {
                    "line": 2,
                    "column": 3
                }
            ],
            "path": ["view_profile"]
        }
    ],
    "data": {
        "view_profile": null
    }
}
```

---
