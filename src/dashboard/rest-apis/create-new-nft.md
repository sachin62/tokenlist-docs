# Create New NFT

> The API allows you to create a new NFT

---

### REST

---

## Headers

| Fields         | Type   | Description                 | Example                              | Data Management |
| -------------- | ------ | --------------------------- | ------------------------------------ | --------------- |
| URL            | string |                             | `{base_url}/creators/create-new-nft` |                 |
| METHOD         | string |                             | **POST**                             |                 |
| content-type   | string | JSON                        | **application/json**                 |                 |
| x-access-token | string | session token with validity | **token**                            | creator token   |

---

## Request

```json
{
    "type_id": "6253ec74fb9f6b511cf05fbf",
    "file": "base64img"
}
```

> contains more fields based on the NFT type

---

## Request Parameters

| Fields     | Type   | Description               | Required                    |
| ---------- | ------ | ------------------------- | --------------------------- |
| type\__id_ | string | \_id of the NFT type      | <div align="center">✔</div> |
| file       | string | img file in base64 format | <div align="center">✘</div> |

---

## Success

```json
{ "message": "NFT created", "_id": "624fc253521415614f508933" }
```

---

## Error

```json
{
    "message": "unauthorized"
}
```

---
