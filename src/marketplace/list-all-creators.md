# Fetch All Creators

> This API is for getting the list of all creators

---

### GRAPHQL

---

## Headers

| Fields       | Type   | Description | Example              | Data Management |
| ------------ | ------ | ----------- | -------------------- | --------------- |
| URL          | string |             | `{base_url}/users`   |                 |
| GRAPHQL      | string |             | **QUERY**            |                 |
| content-type | string | JSON        | **application/json** |                 |

---

## Request

```json
{
  list_all_creators(page_size: 2, page_number: 1) {
    message
    data {
      _id
      name
      email
      img
    }
    hasMore
  }
}

```

---

## Request Parameters

| Fields      | Type    | Description                          | Required                    |
| ----------- | ------- | ------------------------------------ | --------------------------- |
| page_size   | integer | total number of data to be displayed | <div align="center">✘</div> |
| page_number | integer | page number to filter data           | <div align="center">✘</div> |

---

## Success

```json
{
    "data": {
        "list_all_creators": {
            "message": "data listed",
            "data": [
                {
                    "_id": "6277405dc048d34ecb4dc58b",
                    "name": "xjtzli",
                    "email": "paysackhq@gmail.com",
                    "img": null
                },
                {
                    "_id": "6255282ac27cdd85d5cf36d1",
                    "name": "jimshad",
                    "email": "user1@gmail.com",
                    "img": null
                }
            ],
            "hasMore": true
        }
    }
}
```

---

## Error

```json

```

---
