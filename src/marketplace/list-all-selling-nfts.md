# Fetch All Listed NFTs

> This API is for getting the list of all NFTs added for sales

---

### GRAPHQL

---

## Headers

| Fields       | Type   | Description | Example              | Data Management |
| ------------ | ------ | ----------- | -------------------- | --------------- |
| URL          | string |             | `{base_url}/users`   |                 |
| GRAPHQL      | string |             | **QUERY**            |                 |
| content-type | string | JSON        | **application/json** |                 |

---

## Request

```json
{
  list_all_selling_nfts(page_size: 2, page_number: 1) {
    message
    data {
      _id
      amount
      is_sold
      seller_id
    }
    hasMore
  }
}
```

---

## Request Parameters

| Fields      | Type    | Description                          | Required                    |
| ----------- | ------- | ------------------------------------ | --------------------------- |
| page_size   | integer | total number of data to be displayed | <div align="center">✘</div> |
| page_number | integer | page number to filter data           | <div align="center">✘</div> |

---

## Success

```json
{
    "data": {
        "list_all_selling_nfts": {
            "message": "data listed",
            "data": [
                {
                    "_id": "62511d083b5ded111f2bf507",
                    "amount": 40000000,
                    "is_sold": false,
                    "seller_id": "62299c9254f6c2bcd4b2f09a"
                }
            ],
            "hasMore": false
        }
    }
}
```

---

## Error

```json

```

---
