# Fetch All Creator NFTs

> This API is for getting the list of all NFTs created by a creator

---

### GRAPHQL

---

## Headers

| Fields       | Type   | Description | Example              | Data Management |
| ------------ | ------ | ----------- | -------------------- | --------------- |
| URL          | string |             | `{base_url}/users`   |                 |
| GRAPHQL      | string |             | **QUERY**            |                 |
| content-type | string | JSON        | **application/json** |                 |

---

## Request

```json
{
  list_all_creator_nfts(creator_id: "6277405dc048d34ecb4dc58b", page_size: 2, page_number: 1) {
    message
    data {
      creator {
        _id
        name
        email
        img
      }
      arts {
        _id
        name
        art_url
        nft_id
      }
    }
    hasMore
  }
}
```

---

## Request Parameters

| Fields      | Type    | Description                          | Required                    |
| ----------- | ------- | ------------------------------------ | --------------------------- |
| page_size   | integer | total number of data to be displayed | <div align="center">✘</div> |
| page_number | integer | page number to filter data           | <div align="center">✘</div> |
| creator_id  | string  | \_id of the creator                  | <div align="center">✔</div> |

---

## Success

```json
{
    "data": {
        "list_all_creator_nfts": {
            "message": "data listed",
            "data": {
                "creator": {
                    "_id": "62299c9254f6c2bcd4b2f09a",
                    "name": "Sachin T S",
                    "email": "sachinsuresh704@gmail.com",
                    "img": null
                },
                "arts": [
                    {
                        "_id": "6257ac81ba8a2b3e71325ec0",
                        "name": "Boy",
                        "art_url": "https://nft-store-assets.s3.amazonaws.com/b6e367bc-612e-4388-a685-fdf7c0490df1.png",
                        "nft_id": "EsgkMutFq5xa58gGcb86M2uzAdDJbcegzwgGMuiCCMMN"
                    },
                    {
                        "_id": "6257ac1dba8a2bd3d0325e8b",
                        "name": "Man",
                        "art_url": "https://nft-store-assets.s3.amazonaws.com/e8621f14-fcc0-41ad-94a3-13d6297d24e8.png",
                        "nft_id": "ntQXvzKjCyretMDpvAd35Ya3wwzZtWkNRN5UujccsTa"
                    }
                ]
            },
            "hasMore": true
        }
    }
}
```

---

## Error

```json
{
    "errors": [
        {
            "message": "creator not found",
            "locations": [
                {
                    "line": 2,
                    "column": 3
                }
            ],
            "path": ["list_all_creator_nfts"]
        }
    ],
    "data": {
        "list_all_creator_nfts": null
    }
}
```

---
