# Fetch NFT Details

> This API is for getting the details of an NFT

---

### GRAPHQL

---

## Headers

| Fields       | Type   | Description | Example              | Data Management |
| ------------ | ------ | ----------- | -------------------- | --------------- |
| URL          | string |             | `{base_url}/users`   |                 |
| GRAPHQL      | string |             | **QUERY**            |                 |
| content-type | string | JSON        | **application/json** |                 |

---

## Request

```json
{
  list_nft_details(nft_id: "2iRD8A88h1SRkK1Hhra53jqnfEkhXx5mK58PvLb5YTRW") {
    message
    data {
      _id
      name
      description
      update_authority
      art_url
      royalty_percentage
      nft_id
      maximum_supply
      is_for_sale
      selling_price
      owner_id {
        _id
        name
        email
      }
      creator_id {
        _id
        name
        email
      }
    }
  }
}

```

---

## Request Parameters

| Fields | Type   | Description               | Required                    |
| ------ | ------ | ------------------------- | --------------------------- |
| nft_id | string | public address of the NFT | <div align="center">✔</div> |

---

## Success

```json
{
    "data": {
        "list_nft_details": {
            "message": "data listed",
            "data": {
                "_id": "624fc253521415614f508933",
                "name": "Old man",
                "description": "Old man cartoon character",
                "update_authority": "62299c9254f6c2bcd4b2f09a",
                "art_url": "https://nft-store-assets.s3.amazonaws.com/033d711b-5554-47c2-8c7f-41e935cab405.png",
                "royalty_percentage": 0,
                "nft_id": "2iRD8A88h1SRkK1Hhra53jqnfEkhXx5mK58PvLb5YTRW",
                "maximum_supply": null,
                "is_for_sale": false,
                "selling_price": 0,
                "owner_id": {
                    "_id": "62299c9254f6c2bcd4b2f09a",
                    "name": "Sachin T S",
                    "email": "sachinsuresh704@gmail.com"
                },
                "creator_id": {
                    "_id": "62299c9254f6c2bcd4b2f09a",
                    "name": "Sachin T S",
                    "email": "sachinsuresh704@gmail.com"
                }
            }
        }
    }
}
```

---

## Error

```json
{
    "errors": [
        {
            "message": "art not found",
            "locations": [
                {
                    "line": 2,
                    "column": 3
                }
            ],
            "path": ["list_nft_details"]
        }
    ],
    "data": {
        "list_nft_details": null
    }
}
```

---
