# Login With OTP

> This API is for logging into the user account

---

### GRAPHQL

---

## Headers

| Fields       | Type   | Description | Example              | Data Management |
| ------------ | ------ | ----------- | -------------------- | --------------- |
| URL          | string |             | `{base_url}/users`   |                 |
| GRAPHQL      | string |             | **QUERY**            |                 |
| content-type | string | JSON        | **application/json** |                 |

---

## Request

```json
{
  login_with_otp(email: "sachinsuresh704@gmail.com", otp: "12345") {
    message
    token
  }
}

```

---

## Request Parameters

| Fields | Type   | Description               | Required                    |
| ------ | ------ | ------------------------- | --------------------------- |
| email  | string | email of the user         | <div align="center">✔</div> |
| otp    | string | OTP received in the email | <div align="center">✔</div> |

---

## Success

```json
{
    "data": {
        "login_with_otp": {
            "message": "Login success",
            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyMjk5YzkyNTRmNmMyYmNkNGIyZjA5YSIsIm5hbWUiOiJTYWNoaW4gVCBTIiwiYWNjb3VudFR5cGUiOiJjcmVhdG9yIiwiaWF0IjoxNjUyMzMxMjg1LCJleHAiOjE2NTIzNTI4ODV9.W3iuq0X7mSCQTC7c4D38rXIe5NgX0A6LgoIxoMwoJAo"
        }
    }
}
```

---

## Error

```json
{
    "errors": [
        {
            "message": "Incorrect OTP",
            "locations": [
                {
                    "line": 2,
                    "column": 3
                }
            ],
            "path": ["login_with_otp"]
        }
    ],
    "data": {
        "login_with_otp": null
    }
}
```

---
